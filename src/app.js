import React from 'react';
import ReactDOM from 'react-dom';
import {
    BrowserRouter as Router,
    Route
} from 'react-router-dom';
import Login from './login.js';
import Profile from './profile.js';
import Stripe from './main.js';
import {MDCRipple} from '@material/ripple';


const routes = (
    <Router>
        <div>
            <Route path='/' component={Login} exact={true}/>
            <Route path='/main' component={Stripe} exact={true}/>
        </div>
    </Router>
);
class Test extends React.component{
    constructor(props){
        super(props)
    }
    render(){
        return(
            <div>
                <button className='foo-button mdc-button'>TEST</button>
            </div>
        )
    }
}

ReactDOM.render(<Test/>, document.getElementById('app'))