import React from 'react'

class Profile extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            user : {
                username: '',
                first_name: '',
                last_name: '',
                email: ''
            },
            avatar: null
        }
        fetch(document.location.origin.slice(0,-5) + ':8000' + '/api/profile', {
            method: 'get',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.token
            }
        }).then(response => response.json()).then(data => this.setState(data)).catch(error => console.log(error))
    }
    render(){
        return (
            <div>
                <div>{this.state.user.username}</div>
                <div>{this.state.user.first_name}</div>
                <div>{this.state.user.last_name}</div>
                <div>{this.state.user.email}</div>
            </div>
        );
    }
}

export default Profile;